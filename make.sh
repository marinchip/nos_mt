#!/bin/sh

# assemble kernel files, place in link
knl_files="boot commands common filesys jsys jsysf loader \
           rtsched script special terminal user prim edit$"

cd kernel
for i in $knl_files
do
   echo link/$i.rel=kernel/$i.asm
   ../../emu/host 1/asm ../link/$i.rel=$i.asm
done

# assemble driver files, place in link
drv_files="spool printer prntcent batch idedsk rtc9995 term9902 snap"

cd ../drivers
for i in $drv_files
do
   echo link/$i.rel=drivers/$i
   ../../emu/host 1/asm ../link/$i.rel=$i
done

# build configuration files, assemble and place in link
cd ../generation
echo Generating configuration files
../../emu/host 1/basic <<EOF
compile:sysgen.bas
run
../sp-system/sp-emu
bye
EOF

../../emu/host 1/asm ../link/config.rel=config
../../emu/host 1/asm ../link/comdata.rel=comdata

# link the kernel, kernel image is nos.sav
echo Linking kernel...
cd ../link
../../emu/host 1/link <<EOF
out nos.sav
in @l
map
end
EOF

# move kernel image to top dir and clean-up link dir
mv nos.sav ..
rm *.rel


